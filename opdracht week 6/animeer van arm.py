# -*- coding: utf-8 -*-
"""
Created on Tue Oct 11 14:16:48 2022

@author: BeTra
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from simuleer_armen import simuleer_armen

lengte = 0.6 #meter
massa = 11 #kilo
hoek_t0 = 75*np.pi/180 #graden
hoeksnelheid_t0 = -100*np.pi/180 #graden/s
Maximale_spierkracht = 20 #n/m
frequentie_spier_aanspannen = 2 # hz

simulatie_duur = 10 #seconden
interval = .05 #interval in seconden
aantal_samples = int(simulatie_duur/interval)



tijd = np.linspace(0,simulatie_duur,aantal_samples)

def bereken_actuele_spierkracht(x,t):
    return (Maximale_spierkracht*np.cos(2*np.pi*frequentie_spier_aanspannen*t))

def bereken_xdot_1(x,t):
    return(-(3*9.81*np.sin(x))/(2*lengte)+bereken_actuele_spierkracht(x,t))

def df(q,t):
    xdot = [[],[]]
    xdot[0] = q[1]
    xdot[1] = bereken_xdot_1(q[0],t)
    return (xdot[0],xdot[1])

def conditioneer_waarden(waarden):
    waarden = (waarden[:,0]*(180/np.pi))-90
    waarden = waarden.reshape((waarden.size, 1))
    return (list(waarden))
     
print('modeleer systeem')
modelwaarden = odeint(df,([hoek_t0,hoeksnelheid_t0]), tijd)

simulatie = simuleer_armen(lengte)    

simulatie.animeer_arm(conditioneer_waarden(modelwaarden), plot_title='geanimeerde XY plot enkele arm met spierkracht',playback_speed = 50)

plt.figure()
plt.plot(tijd, modelwaarden[:,0]*180/np.pi, label="hoek [Graden]")
plt.plot(tijd, modelwaarden[:,1]*180/np.pi, label="Hoeksnelheid [Graden/s]")
plt.ylabel('hoek in graden')
plt.xlabel('tijd in seconden')
plt.legend()
plt.grid()
plt.show()


