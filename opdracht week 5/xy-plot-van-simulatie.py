# -*- coding: utf-8 -*-
"""
Created on Tue Oct 11 10:04:15 2022

@author: BeTra
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from simuleer_armen import simuleer_armen

lengte = 0.6 #meter
massa = 11 #kilo
hoek_t0 = 75*np.pi/180 #graden
hoeksnelheid_t0 = -100*np.pi/180 #graden/s
Maximale_spierkracht = 20 #n/m
frequentie_spier_aanspannen = 2 # hz

tijd = np.linspace(0,1,100)

def bereken_actuele_spierkracht(x,t):
    return (Maximale_spierkracht*np.cos(2*np.pi*frequentie_spier_aanspannen*t))

def bereken_xdot_1(x,t):
    return(-(3*9.81*np.sin(x))/(2*lengte)+bereken_actuele_spierkracht(x,t))

def df(q,t):
    xdot = [[],[]]
    xdot[0] = q[1]
    xdot[1] = bereken_xdot_1(q[0],t)
    return (xdot[0],xdot[1])

def selecteer_specifieke_posities(modelwaarden, tijd, aantal_opnamen):
    hoekenlijst = modelwaarden[:,0]
    posities = int(len(tijd)/aantal_opnamen)
    x = 0
    hoeken = np.zeros((aantal_opnamen,1))
    while x < hoeken.size:
        print(tijd[(x*posities)])
        hoeken[x] = [(hoekenlijst[(x*posities)]*180/np.pi-90)]
        x+=1
        
    return hoeken.tolist()

modelwaarden = odeint(df,([hoek_t0,hoeksnelheid_t0]), tijd)

simulatie = simuleer_armen(lengte)

hoeken = selecteer_specifieke_posities(modelwaarden, tijd, 6)

plt.plot(tijd, modelwaarden[:,0]*180/np.pi, label="hoek [Graden]")
plt.plot(tijd, modelwaarden[:,1]*180/np.pi, label="Hoeksnelheid [Graden/s]")
plt.ylabel('hoek in graden')
plt.xlabel('tijd in seconden')
plt.legend()
plt.grid()
    

simulatie.teken_armen(hoeken, plot_title='6 waarden van de arm in de simulatie van week 4')


