# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 14:05:01 2020

@author: Laurens Roos
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint

lengte = 0.6 #meter
massa = 11 #kilo
hoek_t0 = 75*np.pi/180 #graden
hoeksnelheid_t0 = -100*np.pi/180 #graden/s
Maximale_spierkracht = 20 #n/m
frequentie_spier_aanspannen = 2 # hz

tijd = np.linspace(0,5,100)


def bereken_xdot_1(x,t):
    return(-(3*9.81*np.sin(x))/(2*lengte))

def df(q,t):
    xdot = [[],[]]
    xdot[0] = q[1]
    xdot[1] = bereken_xdot_1(q[0],t)
    return (xdot[0],xdot[1])

modelwaarden = odeint(df,([hoek_t0,hoeksnelheid_t0]), tijd)


plt.plot(tijd, modelwaarden[:,0]*180/np.pi, label="hoek [Graden]")
plt.plot(tijd, modelwaarden[:,1]*180/np.pi, label="Hoeksnelheid [Graden/s]")
plt.ylabel('hoek in graden')
plt.xlabel('tijd in seconden')
plt.legend()
plt.grid()
    

