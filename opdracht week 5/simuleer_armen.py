# -*- coding: utf-8 -*-
"""
Created on Tue Oct 11 10:06:29 2022

@author: BeTra
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.transforms import Affine2D
import matplotlib.animation as animation

class simuleer_armen():
    __arm_lengte = 0 #arm lengte voor berekeningen
    __hoeken_van_arm = []
    __title = ''
    
    #private functies
    def __init__(self, lengte_armen, default_dikte_armen = 0.1):
        self.__arm_lengte = lengte_armen
        self.__default_dikte_armen = default_dikte_armen
    
    def __formateer_rotatie_matrixen(self, hoek):
        hoek = (hoek/180)*np.pi
        return([[np.cos(hoek),np.sin(hoek)],[-np.sin(hoek),np.cos(hoek)]])
    
    def __bereken_relatieve_coordinaten_van_enkele_arm(self,rotatie_matrix):
         return np.dot(rotatie_matrix, [self.__arm_lengte,0])
     
    def __bereken_absolute_coordinaten_armen(self, rotatie_matrix_armen):
        aantal_rotaties = len(rotatie_matrix_armen)
        aantal_armen = len(rotatie_matrix_armen[0])
        x_y_posities_armen = np.zeros((aantal_rotaties, aantal_armen,2))
        
        for x in range(aantal_rotaties):
            for y in range(aantal_armen):
                if y == 0:
                    rotatie_matrix = self.__formateer_rotatie_matrixen(rotatie_matrix_armen[x][y])
                    x_y_posities_armen[x][y] = self.__bereken_relatieve_coordinaten_van_enkele_arm(rotatie_matrix)
                else:
                    rotatie_matrix = self.__formateer_rotatie_matrixen(rotatie_matrix_armen[x][y])
                    x_y_posities_armen[x][y] =x_y_posities_armen[x][y-1]+self.__bereken_relatieve_coordinaten_van_enkele_arm(rotatie_matrix)
        return x_y_posities_armen               
    def __clear_plot(self, title=''):
        if len(title) != 0:
            self.__title = title
        self.__ax.clear()
        plt.grid(which='both')
        plt.xlim(-2,2)
        plt.ylim(-2,2)

        plt.ylabel('Y positie in meter')
        plt.xlabel('X positie in meter')
        plt.title(self.__title)
    def __animate(self, i, is_absolute = True):
        patches = []
        relatieve_rotatie_coordinaten_voor_tekening = np.array([0, -self.__default_dikte_armen/2])
        begin_positie = np.array([0,0])
        punt_van_rotatie = relatieve_rotatie_coordinaten_voor_tekening
        lijst_van_hoeken = self.__hoeken_van_arm[i]
        hoek = 0
        self.__clear_plot()
        for x in range(len( lijst_van_hoeken )):
            
            if is_absolute == True: 
                hoek = lijst_van_hoeken[x]
            else:
                hoek = hoek + lijst_van_hoeken[x]
                
            self.__ax.plot(begin_positie[0],begin_positie[1], color='r', marker='o', markersize=2)
            rec = plt.Rectangle(punt_van_rotatie, width=self.__arm_lengte, height=self.__default_dikte_armen, 
                    color='b', alpha=0.9,
                    transform=Affine2D().rotate_deg_around(*(begin_positie), hoek)+ self.__ax.transData)
            begin_positie = np.add(begin_positie, (self.__arm_lengte*np.array([np.cos(hoek*(np.pi/180)),np.sin(hoek*(np.pi/180))])))
            punt_van_rotatie = np.add(relatieve_rotatie_coordinaten_voor_tekening, begin_positie)
            patches.append(self.__ax.add_patch(rec))
        return (patches)
        
    
    #public functies
    
    def geef_vector_coordinaten_armen(self, rotatie_lijst, arm_selecte = 0):
        """
        geeft de coordinaten van de armen op basis van de een lijst van rotaties.
        
        
        Parameters
        ----------
        rotatie_lijst : Arrray
            Lijst van rotatie van alle armen die gegeven zijn. in de volgende layout: [[Rotatie_1,Rotatie_2,etc],[Rotatie_1,Rotatie_2,etc],etc]
        arm_selecte : int, optional
            selecteer welke coordinaten er gegeven worden. als deze 0 is worden alle coordinaten gegeven 

        Returns
        -------
        

        """
        if arm_selecte == 0:
            return(self.__bereken_absolute_coordinaten_armen(rotatie_lijst))
        else: 
            return(False)
        
    
    def teken_armen(self, rotatie_lijst, selecteer_arm = 0, is_absolute = True, plot_title = 'X,Y plot verschillende armen'):
        """
        

        Parameters
        ----------
        rotatie_lijst : Numpy Array
            rotatie lijst in format [[[hoek 1],[hoek 2]],[[hoek 1],[hoek2]]]
        selecteer_arm : INT
            selecteer een bepaalde arm om te schetsen, The default is 0.
        is_absolute : bool
            geef aan of de waarde van de hoeken absoluut zijn tenover assen, of relatief zijn tenopzichte van de armen. The default is True.
        plot_title : string
            geef een title aan de plot die getekend word.
        Returns
        -------
        None.

        """
        fig, ax = plt.subplots()
        plt.grid(which='both')
        plt.xlim(-2,2)
        plt.ylim(-2,2)
        dikte_arm = self.__default_dikte_armen
        plt.ylabel('Y positie in meter')
        plt.xlabel('X positie in meter')
        plt.title(plot_title)
        
        for elementen in rotatie_lijst:
            relatieve_rotatie_coordinaten_voor_tekening = np.array([0, -dikte_arm/2])
            begin_positie = np.array([0,0])
            punt_van_rotatie = relatieve_rotatie_coordinaten_voor_tekening
            hoek = 0
            for x in range(len( elementen )):
                if is_absolute == True: 
                    hoek = elementen[x]
                else:
                    hoek = hoek + elementen[x]
                    
                ax.plot(begin_positie[0],begin_positie[1], color='r', marker='o', markersize=2)
                rec = plt.Rectangle(punt_van_rotatie, width=self.__arm_lengte, height=dikte_arm, 
                        color='b', alpha=0.9,
                        transform=Affine2D().rotate_deg_around(*(begin_positie), hoek)+ax.transData)
                begin_positie = np.add(begin_positie, (self.__arm_lengte*np.array([np.cos(hoek*(np.pi/180)),np.sin(hoek*(np.pi/180))])))
                punt_van_rotatie = np.add(relatieve_rotatie_coordinaten_voor_tekening, begin_positie)
                
                ax.add_patch(rec)
        
        plt.show()
        
    def animeer_arm(self, rotatie_lijst, is_absolute = True, plot_title = 'animatie van arm in X,Y plot', playback_speed = 1):
        
        self.__hoeken_van_arm = rotatie_lijst
        self.__fig , self.__ax = plt.subplots()
        
        self.__clear_plot(plot_title)
        
        anim = animation.FuncAnimation(self.__fig, self.__animate,
                           frames=(len(rotatie_lijst)), interval=playback_speed, blit=False)
        anim.save('animation.mp4')
        plt.show()
        
        
        
def setup_test_lijst(start, stop, counter):
           lijst = np.arange(0,90,1)

           lijst = lijst.reshape((len(lijst),1))

           lijst = list(lijst)
           
           return (lijst)
            
def test(start, stop, counter, lengte_arm):
    lijst = setup_test_lijst(start, stop, counter)
    test = simuleer_armen(lengte_arm)
    an = test.animeer_arm(lijst)
            
            